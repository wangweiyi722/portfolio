drop view paypal_view ;
drop view credit_card_view ;


create view paypal_view as 
SELECT 
    user_id,
    user_first_name,
    user_last_name,
    user_phone_number,
    user_email,
    "payment method",
    account_username
    
FROM "user" where "payment method" = 'paypal';

create or replace TRIGGER paypal_trigger
     INSTEAD OF insert ON paypal_view
     FOR EACH ROW
BEGIN
    insert into "user"(
        user_id,
        user_first_name,
        user_last_name,
        user_phone_number,
        user_email,
        "payment method",
        account_username)
    VALUES ( 
        :NEW.user_id,
        :NEW.user_first_name,
        :NEW.user_last_name,
        :NEW.user_phone_number,
        :NEW.user_email,
        'paypal',
        :NEW.account_username) ;
END;
/

create view credit_card_view as 
SELECT 
    user_id,
    user_first_name,
    user_last_name,
    user_phone_number,
    user_email,
    "payment method",
    credit_card_number
    
FROM "user" where "payment method" = 'credit_card' ;

create or replace TRIGGER credit_card_trigger
     INSTEAD OF insert ON credit_card_view
     FOR EACH ROW
BEGIN
    insert into "user"(
        user_id,
        user_first_name,
        user_last_name,
        user_phone_number,
        user_email,
        "payment method",
        account_username)
    VALUES ( 
        :NEW.user_id,
        :NEW.user_first_name,
        :NEW.user_last_name,
        :NEW.user_phone_number,
        :NEW.user_email,
        'credit_card',
        :NEW.credit_card_number) ;
END;
/