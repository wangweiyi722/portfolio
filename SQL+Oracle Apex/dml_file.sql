-- disable triggers
alter trigger make_model_make_model_id_TRG disable;
alter trigger TUI_TUI_id_TRG disable;
alter trigger car_car_id_TRG disable;
alter trigger disability_disability_id_TRG disable;
alter trigger pick_up_loc_loc_id_TRG disable;
alter trigger trip_trip_id_TRG disable;
alter trigger user_user_id_TRG disable;
alter trigger user_disabilities_UD_id_TRG disable;

-- disable constraints
ALTER TABLE car disable CONSTRAINT Relation5;
ALTER TABLE TUI disable CONSTRAINT Relation13;
ALTER TABLE TUI disable CONSTRAINT Relation24;
ALTER TABLE car disable CONSTRAINT Relation27;
ALTER TABLE trip disable CONSTRAINT Relation31;
ALTER TABLE user_disabilities disable CONSTRAINT Relation3;
ALTER TABLE trip disable CONSTRAINT Relation30;
ALTER TABLE user_disabilities disable CONSTRAINT Relation4;
ALTER TABLE car disable CONSTRAINT Relation5;
ALTER TABLE TUI disable CONSTRAINT Relation6;
ALTER TABLE "user" disable CONSTRAINT CH_INH_user;
ALTER TABLE "user" disable CONSTRAINT user_ExDep;


-- truncate tables
truncate table make_model;
truncate table car;
truncate table TUI;
truncate table trip;
truncate table disability;
truncate table user_disabilities;
truncate table "user";
truncate table pick_up_loc;

--Insert into make and model table
insert into make_model(make_model_id, manufacturer, model)
  values(1,'Honda','Civic');
insert into make_model(make_model_id, manufacturer, model)
  values(2,'Toyota','Sienna');
insert into make_model(make_model_id, manufacturer, model)
  values(3,'Hummer','H2');
insert into make_model(make_model_id, manufacturer, model)
  values(4,'Lambhorgini','Huracan');



--Insert into user table
insert into "user"(user_id,user_first_name,user_last_name,user_phone_number,user_email,account_username,credit_card_number,"payment method")
  values(1,'Alex','Avery','713-929-5792','aa@gmail.com','aa@gmail.com',NULL,'paypal');
insert into "user"(user_id,user_first_name,user_last_name,user_phone_number,user_email,account_username,credit_card_number,"payment method")
  values(2,'Barry','Bonds','713-239-7943','bb@gmail.com','bb@gmail.com',NULL,'paypal');
insert into "user"(user_id,user_first_name,user_last_name,user_phone_number,user_email,account_username,credit_card_number,"payment method")
  values(3,'Colin','Cater','713-720-5652','cc@gmail.com',NULL,1298483729,'credit_card');
insert into "user"(user_id,user_first_name,user_last_name,user_phone_number,user_email,account_username,credit_card_number,"payment method")
  values(4,'Dylan','Doyle','713-152-5321','dd@gmail.com',NULL,9382834692,'credit_card');
insert into "user"(user_id,user_first_name,user_last_name,user_phone_number,user_email,account_username,credit_card_number,"payment method")
  values(5,'Evan','Ellison','512-302-1599','ee@gmail.com','ee@gmail.com',NULL,'paypal');
insert into "user"(user_id,user_first_name,user_last_name,user_phone_number,user_email,account_username,credit_card_number,"payment method")
  values(6,'Francis','Flander','512-324-1618','ff@gmail.com',NULL,1956283948,'credit_card');
insert into "user"(user_id,user_first_name,user_last_name,user_phone_number,user_email,account_username,credit_card_number,"payment method")
  values(7,'Gus','Giles','512-817-2873','gg@gmail.com',NULL,942839138,'credit_card');

-- Insert into car table
insert into car(car_id,make_model_id,license_plate,user_id)
  values(1,1,'c68928',1);
insert into car(car_id,make_model_id,license_plate,user_id)
  values(2,2,'b8269c',1); 
insert into car(car_id,make_model_id,license_plate,user_id)
  values(3,2,'a6928b',2);
insert into car(car_id,make_model_id,license_plate,user_id)
  values(4,3,'cp26c8',3);

  
  

-- Insert into disabilities table
insert into disability(disability_id,disability_name)
  values(1,'Diabetes');
insert into disability(disability_id,disability_name)
  values(2,'Asthma');
insert into disability(disability_id,disability_name)
  values(3,'Amputee');
insert into disability(disability_id,disability_name)
  values(4,'Vision Impairment');
insert into disability(disability_id,disability_name)
  values(5,'Hearing Impairment');
insert into disability(disability_id,disability_name)
  values(6,'Claustrophobia');
insert into disability(disability_id,disability_name)
  values(7,'Allergies');
  
-- Insert into user_disabilities
insert into user_disabilities(UD_id,user_id,disability_id)
  values(1,1,7);
insert into user_disabilities(UD_id,user_id,disability_id)
  values(2,1,5);
insert into user_disabilities(UD_id,user_id,disability_id)
  values(3,2,7);
insert into user_disabilities(UD_id,user_id,disability_id)
  values(4,5,7);
insert into user_disabilities(UD_id,user_id,disability_id)
  values(5,6,6);
  
-- Insert into pick_up_loc
insert into pick_up_loc(loc_id,house_number,street_name,city,state,zip_code)
  values(1,1234,'Fake Street','Houston','TX',77777);
insert into pick_up_loc(loc_id,house_number,street_name,city,state,zip_code)
  values(2,5678,'Imaginary Lane','Houston','TX',88888);
insert into pick_up_loc(loc_id,house_number,street_name,city,state,zip_code)
  values(3,2468,'False Way','Dallas','TX',66666);
insert into pick_up_loc(loc_id,house_number,street_name,city,state,zip_code)
  values(4,1357,'Madeup Boulevard','Dallas','TX',55555);
insert into pick_up_loc(loc_id,house_number,street_name,city,state,zip_code)
  values(5,9876,'Fabrication Drive','Austin','TX',44444);
insert into pick_up_loc(loc_id,house_number,street_name,city,state,zip_code)
  values(6,1248,'Nonexistent Parkway','Austin','TX',33333);
  
-- Insert into trip table
insert into trip(trip_id,origin,destination,date_time,capacity,cost,user_id,car_id)
  values(1,'Houston','Dallas','11-30-2016',4,10,1,2);
insert into trip(trip_id,origin,destination,date_time,capacity,cost,user_id,car_id)
  values(2,'Houston','Austin','12-10-2016',3,10,1,1);
insert into trip(trip_id,origin,destination,date_time,capacity,cost,user_id,car_id)
  values(3,'Dallas','Houston','12-4-2016',2,15,2,3);
insert into trip(trip_id,origin,destination,date_time,capacity,cost,user_id,car_id)
  values(4,'Austin','Houston','12-2-2016',4,10,1,2);
  insert into trip(trip_id,origin,destination,date_time,capacity,cost,user_id,car_id)
  values(5,'Austin','Houston','11-20-2016',3,10,3,4);
  
 

-- Insert into TUI table
insert into TUI(TUI_id,user_id,trip_id,loc_id)
  values(1,3,4,5);
insert into TUI(TUI_id,user_id,trip_id,loc_id)
  values(2,5,4,5);
insert into TUI(TUI_id,user_id,trip_id,loc_id)
  values(3,6,4,5);
insert into TUI(TUI_id,user_id,trip_id,loc_id)
  values(4,7,4,5);
  
insert into TUI(TUI_id,user_id,trip_id,loc_id)
  values(5,3,3,3);
  
insert into TUI(TUI_id,user_id,trip_id,loc_id)
  values(6,4,2,2);
  
insert into TUI(TUI_id,user_id,trip_id,loc_id)
  values(7,6,1,1);

  
  

--Enable triggers
alter trigger make_model_make_model_id_TRG enable;
alter trigger TUI_TUI_id_TRG enable;
alter trigger car_car_id_TRG enable;
alter trigger disability_disability_id_TRG enable;
alter trigger pick_up_loc_loc_id_TRG enable;
alter trigger trip_trip_id_TRG enable;
alter trigger user_user_id_TRG enable;
alter trigger user_disabilities_UD_id_TRG enable;


--Enable constraints
ALTER TABLE car enable CONSTRAINT Relation5;
ALTER TABLE TUI enable CONSTRAINT Relation13;
ALTER TABLE TUI enable CONSTRAINT Relation24;
ALTER TABLE car enable CONSTRAINT Relation27;
ALTER TABLE trip enable CONSTRAINT Relation31;
ALTER TABLE user_disabilities enable CONSTRAINT Relation3;
ALTER TABLE trip enable CONSTRAINT Relation30;
ALTER TABLE user_disabilities enable CONSTRAINT Relation4;
ALTER TABLE car enable CONSTRAINT Relation5;
ALTER TABLE TUI enable CONSTRAINT Relation6;
ALTER TABLE "user" enable CONSTRAINT CH_INH_user;
ALTER TABLE "user" enable CONSTRAINT user_ExDep;